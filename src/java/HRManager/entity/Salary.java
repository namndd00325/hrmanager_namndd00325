/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HRManager.entity;

/**
 *
 * @author quanglinh
 */
public class Salary {
    private long salaryID;
    private int salary;
    private long employeeID;

    public long getSalaryID() {
        return salaryID;
    }

    public void setSalaryID(long salaryID) {
        this.salaryID = salaryID;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public long getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(long employeeID) {
        this.employeeID = employeeID;
    }

    public Salary() {
    }

    public Salary(long salaryID, int salary, long employeeID) {
        this.salaryID = salaryID;
        this.salary = salary;
        this.employeeID = employeeID;
    }
    
}
