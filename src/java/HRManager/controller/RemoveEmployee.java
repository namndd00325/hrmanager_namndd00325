/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HRManager.controller;

import HRManager.model.EmployeeBO;
import HRManager.entity.Employee;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author quanglinh
 */
public class RemoveEmployee extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            response.sendRedirect("login.jsp");
            return;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") != null) {
            String id = request.getParameter("id");
            EmployeeBO ebo = new HRManager.model.EmployeeBO();
            Employee e = ebo.getByID(id);

            if (e == null) {
                response.sendRedirect("InformationPage.jsp?type=error&msg=No have this EmployeeID");
            } else {
                if (ebo.delete(id) > 0) {
                    response.sendRedirect("InformationPage.jsp?type=info&msg=This EmployeeID deleted");
                } else {
                    response.sendRedirect("InformationPage.jsp?type=warning&msg=This EmployeeID can't delete");
                }
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
