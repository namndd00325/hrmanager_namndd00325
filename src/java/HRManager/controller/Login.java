/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HRManager.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import HRManager.entity.User;
import HRManager.model.UserBO;
import javax.servlet.RequestDispatcher;

public class Login extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") != null) {
            response.sendRedirect("employeeManager.jsp");
            return;
        }

    }

    private void goPage(HttpServletRequest request, HttpServletResponse response, String link) throws ServletException, IOException {
        RequestDispatcher reqdis = request.getRequestDispatcher(link);
        reqdis.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            String userName = request.getParameter("txtUserName");
            String userPassword = request.getParameter("txtPassword");
            if (userName.length() == 0) {
                goPage(request, response, "login.jsp?error=UserName");
            } else if (userPassword.length() == 0) {
                goPage(request, response, "login.jsp?error=Password");
            } else {
                User u = new User();
                UserBO ubo = new UserBO();
                u.setUserName(userName);
                u.setUserPassword(userPassword);
                if (ubo.authorization(u)) {
                    session = request.getSession();
                    session.setAttribute("username", u.getUserName());
                    response.sendRedirect("employeeManager.jsp");
                } else {
                    goPage(request, response, "login.jsp?error=Error");
                }
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
