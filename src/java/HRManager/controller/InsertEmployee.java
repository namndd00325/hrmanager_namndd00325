/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HRManager.controller;

import HRManager.model.EmployeeBO;
import HRManager.entity.Employee;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class InsertEmployee extends HttpServlet {

    EmployeeBO ebo = new HRManager.model.EmployeeBO();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            response.sendRedirect("login.jsp");
            return;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);

        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") != null) {
            Employee e;
            String action = request.getParameter("action");
            String id = request.getParameter("id");
            String fName = request.getParameter("txtFirstName");
            String lName = request.getParameter("txtLastName");

            if (lName == null || fName == null) {
                if (action == null || action.equals("add")) {
                    e = new Employee();
                } else {
                    e = ebo.getByID(id);
                }
                request.setAttribute("e", e);
                request.setAttribute("action", action);
                request.setAttribute("id", id);
                request.getRequestDispatcher("processEmployee.jsp").forward(request, response);
                return;
            }
        }
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        processRequest(request, response);

        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") != null) {
            Employee e = new Employee();
            String fName = request.getParameter("txtFirstName");
            String lName = request.getParameter("txtLastName");
            String birthday = request.getParameter("txtBirthDate");
            String hireday = request.getParameter("txtHireDate");

            e.setEmployeeID(System.currentTimeMillis());
            e.setFirstName(fName);
            e.setLastName(lName);
            e.setBirthDate(birthday);
            e.setHireDate(hireday);
            e.setAddress(request.getParameter("txtAddress"));
            e.setCity(request.getParameter("txtCity"));
            e.setCountry(request.getParameter("txtCountry"));
            e.setEmail(request.getParameter("txtEmail"));
            e.setHomePhone(request.getParameter("txtHomePhone"));
            e.setMobile(request.getParameter("txtMobile"));
            e.setNote(request.getParameter("txtNote"));
            
            if (ebo.add(e) > 0) {
                response.sendRedirect("InformationPage.jsp?type=info&msg=This EmployeeID inserted to database");
            } else {
                response.sendRedirect("InformationPage.jsp?type=warning&msg=This EmployeeID can't insert to database");
            }
        }
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
