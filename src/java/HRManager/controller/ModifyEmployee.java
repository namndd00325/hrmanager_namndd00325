/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HRManager.controller;

import HRManager.entity.Employee;
import HRManager.model.EmployeeBO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author quanglinh
 */
public class ModifyEmployee extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            response.sendRedirect("login.jsp");
            return;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
        
        String action = request.getParameter("action");
        String id = request.getParameter("id");
        String fName = request.getParameter("txtFirstName");
        String lName = request.getParameter("txtLastName");
        String birthday = request.getParameter("txtBirthDate");
        String hireday = request.getParameter("txtHireDate");
        EmployeeBO ebo = new HRManager.model.EmployeeBO();
        Employee e = new HRManager.entity.Employee();
        
        e.setEmployeeID(Long.parseLong(id));
        e.setFirstName(fName);
        e.setLastName(lName);
        e.setBirthDate(birthday);
        e.setHireDate(hireday);
        e.setAddress(request.getParameter("txtAddress"));
        e.setCity(request.getParameter("txtCity"));
        e.setCountry(request.getParameter("txtCountry"));
        e.setEmail(request.getParameter("txtEmail"));
        e.setHomePhone(request.getParameter("txtHomePhone"));
        e.setMobile(request.getParameter("txtMobile"));
        e.setNote(request.getParameter("txtNote"));
        if (action.equals("edit")) {
            if (ebo.edit(e) > 0) {
                response.sendRedirect("InformationPage.jsp?type=info&msg=This EmployeeID updated");
            } else {
                response.sendRedirect("InformationPage.jsp?type=warning&msg=This EmployeeID can't update");
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
