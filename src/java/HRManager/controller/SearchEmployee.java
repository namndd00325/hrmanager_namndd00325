/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HRManager.controller;

import HRManager.model.EmployeeBO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SearchEmployee extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            response.sendRedirect("login.jsp");
            return;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") != null) {
            String value = "";
            String option = "";
            EmployeeBO ebo = new HRManager.model.EmployeeBO();
            value = request.getParameter("txtValue");
            option = request.getParameter("ddlSearch");
            System.out.println("option" + option);

            if (value == null || option == null) {
                System.out.println("not");
                RequestDispatcher reqdis = request.getRequestDispatcher("employeeManager.jsp");
                reqdis.forward(request, response);
            } else {
                System.out.println("has");
                response.sendRedirect("employeeManager.jsp?option=" + option + "&value=" + value);
            }
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
