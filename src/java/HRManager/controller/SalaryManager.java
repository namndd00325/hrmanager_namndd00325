/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HRManager.controller;

import HRManager.entity.Employee;
import HRManager.entity.Salary;
import HRManager.model.EmployeeBO;
import HRManager.model.SalaryBO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author quanglinh
 */
public class SalaryManager extends HttpServlet {

    EmployeeBO ebo = new EmployeeBO();
    SalaryBO sbo = new SalaryBO();
    Employee e;
    Salary s;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            response.sendRedirect("login.jsp");
            return;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") != null) {

            String action = request.getParameter("action");
            String id = request.getParameter("id");
            e = ebo.getByID(id);
            if (e != null) {
                request.setAttribute("e", e);
                request.setAttribute("action", action);
                request.setAttribute("id", id);
                request.getRequestDispatcher("salaryAdjustments.jsp").forward(request, response);
                return;
            } else {
                response.sendRedirect("salary.jsp");
                return;
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NumberFormatException {
        processRequest(request, response);

        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") != null) {
            String id = request.getParameter("id");
            int salary = Integer.parseInt(request.getParameter("salary"));
            Long employeeID = Long.parseLong(id);
            
            System.out.println("String id: "+id);
            if ((s = sbo.getByID(id)) != null) {
                s.setSalary(salary);
                if (sbo.edit(s) > 0) {
                    response.sendRedirect("InformationPage.jsp?type=info&msg=This SalaryID updated");
                } else{
                    response.sendRedirect("InformationPage.jsp?type=warning&msg=This SalaryID can't update");
                }
                return;
            } else {
                s = new Salary();
                s.setSalaryID(System.currentTimeMillis());
                s.setSalary(salary);
                s.setEmployeeID(employeeID);
                
                if (sbo.add(s) > 0) {
                    response.sendRedirect("InformationPage.jsp?type=info&msg=This SalaryID inserted to database");
                } else{
                    response.sendRedirect("InformationPage.jsp?type=warning&msg=This SalaryID can't insert to database");
                }
                return;
            }

        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
