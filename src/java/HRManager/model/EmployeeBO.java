/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HRManager.model;

//import HRManager.ConvertData;
import HRManager.utility.ValidData;
import HRManager.dao.DAO;
import HRManager.entity.Employee;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmployeeBO {

    PreparedStatement ps;
    DAO dao = new DAO();

    /**
     *
     * add new Employee to database
     */
    public int add(Employee e) {
        String sql = "INSERT INTO TBL_EMPLOYEE (LastName, FirstName, BirthDay, HireDate, Address, City, Country, "
                + "HomePhone, Mobile, Email, Note, ID_EMP) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        try {

            ps = dao.getConnection().prepareStatement(sql);
            ps.setString(1, e.getLastName());
            ps.setString(2, e.getFirstName());
            ps.setString(3, e.getBirthDate());
            ps.setString(4, e.getHireDate());
            ps.setString(5, e.getAddress());
            ps.setString(6, e.getCity());
            ps.setString(7, e.getCountry());
            ps.setString(8, e.getHomePhone());
            ps.setString(9, e.getMobile());
            ps.setString(10, e.getEmail());
            ps.setString(11, e.getNote());
            ps.setLong(12, e.getEmployeeID());
            System.out.println("add query" + sql);
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeBO.class.getName()).log(Level.SEVERE, null, ex);
        }
//        System.out.println("EmployeeBO.add(e)"+dao.executeUpdateQuery(ps)); poor u
        return dao.executeUpdateQuery(ps);

    }

    /**
     *
     * delete Employee into database
     */
    public int delete(String id) {
        String sql = "DELETE FROM Employees WHERE EmployeeID=?";
        try {
            System.out.println("EmployeeBO.delete(ID): "+dao.getConnection());
            ps = dao.getConnection().prepareStatement(sql);
            ps.setLong(1, Long.parseLong(id));

        } catch (SQLException ex) {
            Logger.getLogger(EmployeeBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dao.executeUpdateQuery(ps);
    }

    /**
     * OK
     * update old Employee by new Employee into database
     */
    public int edit(Employee e) {
        String sql = "UPDATE Employees SET LastName=?, FirstName=?, BirthDate=?, HireDate=?, Address=?, City=?, Country=?,"
                + "HomePhone=?, Mobile=?, Email=?, PhotoPath=?, Note=? WHERE EmployeeID=?";
        try {

            ps = dao.getConnection().prepareStatement(sql);
            ps.setString(1, e.getLastName());
            ps.setString(2, e.getFirstName());
            ps.setString(3, e.getBirthDate());
            ps.setString(4, e.getHireDate());
            ps.setString(5, e.getAddress());
            ps.setString(6, e.getCity());
            ps.setString(7, e.getCountry());
            ps.setString(8, e.getHomePhone());
            ps.setString(9, e.getMobile());
            ps.setString(10, e.getEmail());
            ps.setString(11, e.getPhotoPath());
            ps.setString(12, e.getNote());
            ps.setLong(13, e.getEmployeeID());

        } catch (SQLException ex) {
            Logger.getLogger(EmployeeBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dao.executeUpdateQuery(ps);
    }

    /**
     * OK
     * Lay ve tat ca cac Employee co trong CSDL.
     */
    public List<Employee> select() {
        List<Employee> list = new LinkedList();
        try {
            String sql = "SELECT * FROM Employees";
            ps = dao.getConnection().prepareCall(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Employee e = setValueForEmployee(rs);
                list.add(e);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
//            dao.closeConnection();
            return null;
        }
//        dao.closeConnection();
        return list;
    }

    /**
     * OK
     * get Employee by EmployeeID
     */
    public Employee getByID(String id) {
        try {
            String sql = "SELECT * FROM Employees WHERE EmployeeID=?";
            ps = dao.getConnection().prepareStatement(sql);
            ps.setLong(1, Long.parseLong(id));
            ResultSet rs = ps.executeQuery();
            
//            System.out.println(""+rs.next()); // poor u
            while (rs.next()) {
                Employee e = setValueForEmployee(rs);
//                dao.closeConnection(); // poor u
                return e;
            }
        } catch (SQLException ex) {
            dao.closeConnection();
            Logger.getLogger(EmployeeBO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        dao.closeConnection();
        return null;
    }
    

    /**
     *
     * Tim kiem Employee co trong CSDL.
     */
    public List<Employee> find(int option, String value) {
        String sql = "";
        switch (option) {
            case 0:
                sql = "SELECT * FROM Employees WHERE firstname LIKE ? OR lastname LIKE ? OR"
                        + " lower(firstname) LIKE ? OR lower(lastname) LIKE ?";
                System.out.println("case 0");
                try {
                    ps = dao.getConnection().prepareStatement(sql);
                    ps.setString(1, value+"%");
                    ps.setString(2, value+"%");
                    ps.setString(3, "%"+value+"%");
                    ps.setString(4, "%"+value+"%");
                } catch (SQLException ex) {
                    Logger.getLogger(EmployeeBO.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            case 1:
                sql = "SELECT * FROM Employees WHERE city LIKE ? OR lower(city) LIKE ?";
                System.out.println("case 1");
                try {
                    ps = dao.getConnection().prepareStatement(sql);
                    ps.setString(1, value+"%");
                    ps.setString(2, "%"+value+"%");
                } catch (SQLException ex) {
                    Logger.getLogger(EmployeeBO.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }

        List<Employee> list = new LinkedList();
        try {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                System.out.println("rs.next()");
                Employee e = setValueForEmployee(rs);
                list.add(e);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            dao.closeConnection();
            return null;
        }
        dao.closeConnection();
        return list;
    }
    
    public Employee setValueForEmployee(ResultSet rs) throws SQLException{
        Employee e = new Employee();
        e.setEmployeeID(rs.getLong("EmployeeID"));
        e.setLastName(rs.getString("LastName"));
        e.setFirstName(rs.getString("FirstName"));
        e.setBirthDate(rs.getString("BirthDate"));
        e.setHireDate(rs.getString("HireDate"));
        e.setAddress(rs.getString("Address"));
        e.setCity(rs.getString("City"));
        e.setCountry(rs.getString("Country"));
        e.setHomePhone(rs.getString("HomePhone"));
        e.setMobile(rs.getString("Mobile"));
        e.setEmail(rs.getString("Email"));
        e.setPhotoPath(rs.getString("PhotoPath"));
        e.setNote(rs.getString("Note"));
        return e;
    }

}
