/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HRManager.model;

import HRManager.dao.DAO;
import HRManager.entity.Salary;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author quanglinh
 */
public class SalaryBO {
    PreparedStatement ps;
    DAO dao = new DAO();
    
    /**
     *
     * add new Salary to database
     */
    public int add(Salary s) {
        String sql = "INSERT INTO salarymanager (salaryid, salary, employeeid) VALUES(?,?,?)";
        try {
            System.out.println("s.getSalaryID(): "+s.getSalaryID());
            System.out.println("s.getSalary(): "+s.getSalary());
            System.out.println("s.getEmployeeID(): "+s.getEmployeeID());
            
            ps = dao.getConnection().prepareStatement(sql);
            ps.setLong(1, s.getSalaryID());
            ps.setInt(2, s.getSalary());
            ps.setLong(3, s.getEmployeeID());
        } catch (Exception ex) {
            Logger.getLogger(EmployeeBO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
        return dao.executeUpdateQuery(ps);

    }
    /**
     * OK
     * get all Salary of Employees.
     */
    public List<Salary> select() {
        List<Salary> list = new LinkedList();
        try {
            String sql = "SELECT * FROM SALARYMANAGER";
            ps = dao.getConnection().prepareCall(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Salary s = new Salary();
                s.setSalaryID(rs.getLong("salaryID"));
                s.setSalary(rs.getInt("salary"));
                s.setEmployeeID(rs.getLong("EMPLOYEEID"));
                list.add(s);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
//            dao.closeConnection();
            return null;
        }
//        dao.closeConnection();
        return list;
    }
    
    /**
     * OK
     * get Salary by EmployeeID
     */
    public Salary getByID(String id) {
        try {
            String sql = "SELECT * FROM salarymanager WHERE employeeid=?";
            ps = dao.getConnection().prepareStatement(sql);
            ps.setLong(1, Long.parseLong(id));
            
            System.out.println("String id: "+id);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                Salary s = new Salary();
                s.setSalaryID(rs.getLong("salaryID"));
                s.setSalary(rs.getInt("salary"));
                s.setEmployeeID(rs.getLong("EMPLOYEEID"));
                return s;
            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeBO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
//        dao.closeConnection();
        return null;
    }
    /**
     * 
     * update old Salary by new Salary into database
     */
    public int edit(Salary s) {
        String sql = "UPDATE SALARYMANAGER SET salary=? WHERE EMPLOYEEID=?";
        try {

            ps = dao.getConnection().prepareStatement(sql);
            ps.setLong(1, s.getSalary());
            ps.setLong(2, s.getEmployeeID());

        } catch (Exception ex) {
            Logger.getLogger(EmployeeBO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
        return dao.executeUpdateQuery(ps);
    }
}
