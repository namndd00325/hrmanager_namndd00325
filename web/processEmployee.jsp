
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.username}"> 
    <jsp:forward page="login.jsp"/>
</c:if>

<jsp:include page="header.jsp"/>
<!-- Start content -->

<c:choose>
    <c:when test="${not empty sessionScope.username}">
        <c:set var="lB" value="" scope="request"/>
        <c:choose>
            <c:when test="${empty requestScope.action or requestScope.action eq 'add'}">
                <c:set var="lB" scope="request" value="Add New"/>
            </c:when>
            <c:when test="${requestScope.action eq 'delete'}">
                <c:set var="lB" scope="request" value="Delete"/>
            </c:when>
            <c:when test="${requestScope.action eq 'edit'}">
                <c:set var="lB" scope="request" value="Update"/>
            </c:when>
        </c:choose>
    </c:when>
</c:choose>

<div class="content">
    <div class="container-fluid">
        <h3 align="center">${lB} Employee</h3>

        <jsp:useBean id="e" class="HRManager.entity.Employee" scope="request"/>
        <c:if test = "${lB == 'Add New'}">
            <c:set var = "actionForm" scope = "session" value = "ProcessEmployee"/>
        </c:if>
        <c:if test = "${lB == 'Update'}">
            <c:set var = "actionForm" scope = "session" value = "modifyEmp"/>
        </c:if>
        
        <form class="row" name="frmEmployee" method="post" action="${actionForm}" >
            <div class="col-md-6">
                <div class="form-group">
                    <label for="firstName">Last Name</label>
                    <input required type="text" class="form-control" id="firstName" name="txtFirstName" value='${e.firstName}' >
                </div>

                <div class="form-group">
                    <label for="lastName">First Name</label>
                    <input required type="text" class="form-control" id="lastName" name="txtLastName" value='${e.lastName}' >
                </div>

                <div class="form-group">
                    <label for="birthday">Birthday</label>
                    <input required type="datetime" class="form-control" id="birthday" name="txtBirthDate"  value='${e.birthDate}' >
                </div>

                <div class="form-group">
                    <label for="hireday">Hire Day</label>
                    <input required type="datetime" class="form-control" id="hireday" name="txtHireDate" value='${e.hireDate}'  >
                </div>

                <div class="form-group">
                    <label for="address">Address</label>
                    <input required type="text" class="form-control" id="address" name="txtAddress" value='${e.address}'>
                </div>

                <div class="form-group">
                    <label for="city">City</label>
                    <input required type="text" class="form-control" id="city" name="txtCity" value='${e.city}'>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="country">Country</label>
                    <input required type="text" class="form-control" id="country" name="txtCountry" value='${e.country}'>
                </div>

                <div class="form-group">
                    <label for="homePhone">Home Phone</label>
                    <input required type="text" class="form-control" id="homePhone" name="txtHomePhone" value='${e.homePhone}' >
                </div>

                <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input required type="text" class="form-control" id="mobile" name="txtMobile" value='${e.mobile}' >
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input required type="email" class="form-control" id="email" name="txtEmail" value='${e.email}' >
                </div>

                <div class="form-group">
                    <label for="city">Note</label>
                    <input type="text" class="form-control" id="note" name="txtNote" value='${e.note}'>
                </div>

                <div class="form-group">
                    <input class="btn btn-primary btnAction" type="submit" value='${lB}'>
                </div>

                <input type="hidden" name="action" value="${action}"/>
                <input type="hidden" name="id" value="${requestScope.id}"/>

            </div>

        </form>
    </div>

</div>  

<!-- END content -->
<jsp:include page="footer.jsp"/>