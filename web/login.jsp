
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty sessionScope.username}"> 
    <jsp:forward page="employeeManager.jsp"/>
</c:if>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Login - Employee Manager</title>
        <meta name="description" content="Employee Manager - LG, employee">
        <meta name="author" content="NamND16, NamND16̀, LG, John Baron">

        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
        <link href="css/style.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <jsp:useBean id="valid" class="HRManager.utility.ValidData" scope="session"/>
        <div class="container">
            <form class="col-sm-8 col-md-5 mx-auto formLogin" method="POST" action="Login">
                <div class="form-group">
                    <label for="ipUsername">Username</label>
                    <input type="text" class="form-control" id="ipUsername" name="txtUserName" placeholder="username">
                    <c:if test="${not empty param.error and param.error eq 'UserName'}">
                        <br/>Not null...
                    </c:if>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your account with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="ipPassword">Password</label>
                    <input type="password" class="form-control" id="ipPassword" name="txtPassword" placeholder="password"> 
                    <c:if test="${not empty param.error and param.error eq 'Password'}">
                        <br/>Password incorrect!
                    </c:if>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
                <c:if test="${not empty param.error}">
                    <div class="alert alert-danger" role="alert">
                        Username or password invalid!
                    </div>
                </c:if> 
                <div class="form-check">
                    <button type="submit" name="btnLogin" class="btn btn-primary btnLogin">Login</button>
                </div>
            </form>
        </div>

    </body>
</html>