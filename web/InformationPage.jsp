
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.username}"> 
    <jsp:forward page="login.jsp"/>
</c:if>

<jsp:include page="header.jsp"/>
<!-- Start content -->

<div class="content">
    <div class="container-fluid">
        <c:choose>

            <c:when test="${param.type eq 'error'}">
                <div class="alert alert-danger" role="alert">
                    <h4 class="error">${param.msg}</h4>
                </div>
            </c:when>

            <c:when test="${param.type eq 'info'}">
                <div class="alert alert-success" role="alert">
                    <h1>${param.msg}</h1>
                </div>
            </c:when>

            <c:when test="${type eq 'warning'}">
                <div class="alert alert-warning" role="alert">
                    <h1>${param.msg}</h1>
                </div>
            </c:when>

        </c:choose>
    </div>
</div>

<!-- END content -->
<jsp:include page="footer.jsp"/>