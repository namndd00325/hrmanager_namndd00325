
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.username}"> 
    <jsp:forward page="login.jsp"/>
</c:if>

<jsp:include page="header.jsp"/>
<!-- Start content -->

<div class="content">
    <c:if test="${not empty sessionScope.username}">

        <jsp:useBean id="ebo" class="HRManager.model.EmployeeBO" scope="request"/>
        <jsp:useBean id="sbo" class="HRManager.model.SalaryBO" scope="request"/>
        <c:set var="list" value="${ebo.select()}" scope="request"/>

        <div class="container-fluid">  
            <h3 align="center">Salary Manager</h3>
            <table class="table table-bordered mx-auto">
                <thead>
                    <tr>
                        <th>Employee Code</th>
                        <th>Full Name</th>
                        <th>Birthday</th>
                        <th>Hire Date</th>
                        <th>Salary</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:if test="${fn:length(list) > 0}">
                        <c:forEach items="${list}" var="em">
                            <c:set var="salary" value="${sbo.getByID(em.employeeID).salary}" scope="request"/>
                            <tr>
                                <td>${em.employeeID}</td>
                                <td>${em.firstName} ${em.lastName}</td>
                                <td align='center'>${em.birthDate}</td>
                                <td align='center'>${em.hireDate}</td>
                                <td align='center'>${(salary != null) ? salary : "Please adjust salary!"}</td>
                                <td align='center'><a class="btn btn-outline-primary" href='salaryManager?id=${em.employeeID}'>Adjust</a></td>
                            </tr>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>

        </div> 
    </c:if>
</div>

<!-- END content -->
<jsp:include page="footer.jsp"/>