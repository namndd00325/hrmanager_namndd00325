
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.username}"> 
    <jsp:forward page="login.jsp"/>
</c:if>

<jsp:include page="header.jsp"/>
<!-- Start content -->

<div class="content">
    <c:choose>
        <c:when test="${not empty sessionScope.username}">

            <jsp:useBean id="ebo" class="HRManager.model.EmployeeBO" scope="request"/>
            <c:choose>
                <c:when test="${empty param.option or empty param.value}">
                    <c:set var="list" value="${ebo.select()}" scope="request"/>
                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${param.option eq 'Name'}">
                            <c:set var="list" value="${ebo.find(0, param.value)}" scope="request"/>
                        </c:when>
                        <c:when test="${param.option eq 'City'}">
                            <c:set var="list" value="${ebo.find(1, param.value)}" scope="request"/>
                        </c:when>
                    </c:choose>
                </c:otherwise>
            </c:choose>
            <div class="container-fluid">  
                <h3 align="center" >All employee</h3>
                <table class="table table-bordered mx-auto">
                    <thead>
                        <tr>
                            <!--<th>Employee Code</th>-->
                            <th>Full Name</th>
                            <th>Birthday Date</th>
                            <th>Hire Date</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Country</th>
                            <th>Home Phone</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th colspan="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%--<jsp:useBean id="convert" scope="page" class="HRManager.ConvertData"/>--%>
                        <c:if test="${fn:length(list) > 0}">
                            <c:forEach items="${list}" var="em">
                                <tr>
                                    <!--<td>${em.employeeID}</td>-->
                                    <td>${em.firstName} ${em.lastName}</td>
                                    <td>${em.birthDate}</td>
                                    <td>${em.hireDate}</td>
                                    <td>${em.address}</td>
                                    <td>${em.city}</td>
                                    <td>${em.country}</td>
                                    <td>${em.homePhone}</td>
                                    <td>${em.mobile}</td>
                                    <td>${em.email}</td>
                                    <td align='center'><a class="btn btn-outline-primary" href='ProcessEmployee?action=edit&id=${em.employeeID}'> Edit</a></td>
                                    <td align='center'><a class="btn btn-outline-danger" href='removeEmp?&id=${em.employeeID}'> Delete</a></td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </tbody>
                </table>

            </div> 
        </c:when>
    </c:choose>
</div>

<!-- END content -->
<jsp:include page="footer.jsp"/>