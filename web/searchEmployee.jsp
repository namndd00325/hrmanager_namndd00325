
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.username}"> 
    <jsp:forward page="login.jsp"/>
</c:if>
    
<jsp:include page="header.jsp"/>
<!-- Start content -->

<div class="content">
    <div class="container-fluid">
        <h3 align="center">Search Employee</h3>
        <form class="col-md-6 mx-auto" name="frmSearch" method="POST" action="searchEmployee">

            <div class="form-group">
                <label for="sValue">Search Value</label>
                <input class="form-control" id="sValue" type="text" name="txtValue" placeholder="eg: quang linh">
            </div>

            <div class="form-group">
                <label for="option">Example select</label>
                <select class="form-control" id="option" name="ddlSearch">
                    <option>Name</option>
                    <option>City</option>
                </select>
            </div>

            <div class="form-group">
                <input class="btn btn-primary btn-block btn-search" type="submit" name="btnSearch" value="Search">
            </div>

        </form>
    </div>
</div>

<!-- END content -->
<jsp:include page="footer.jsp"/>