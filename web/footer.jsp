
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.username}"> 
    <jsp:forward page="login.jsp"/>
</c:if>

</div>
<!-- END content-page -->

<footer class="footer">
    <span class="text-right">
        Copyright <a target="_blank">My Website</a>
    </span>
    <span class="float-right">
        Powered by <a target="_blank"><b>John Baron</b></a>
    </span>
</footer>

</div>
<!-- END main -->

<script src="assets/js/modernizr.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/moment.min.js"></script>

<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>

<!-- App js -->
<script src="assets/js/pikeadmin.js"></script>

<!-- BEGIN Java Script for this page -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

<!-- Check validator for form add/edit -->
<!--<script src="script/valid_data.js"></script>-->

<!-- Counter-Up-->
<script src="assets/plugins/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="assets/plugins/counterup/jquery.counterup.min.js"></script>
<script>
    var path = (window.location.pathname).split("/",3);
    console.log(path[2]);
    switch(path[2]){
        case "employeeManager.jsp":
            $('#sidebar-menu ul li:nth-child(2)').addClass('active');
            break;
        case "processEmployee.jsp":
            $('#sidebar-menu ul li:nth-child(3)').addClass('active');
            break;
        case "salary.jsp":
            $('#sidebar-menu ul li:nth-child(4)').addClass('active');
            break;
        case "searchEmployee.jsp":
            $('#sidebar-menu ul li:nth-child(5)').addClass('active');
            break;
        default:
            break;
        
    }
</script>

</body>
</html>