

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.username}"> 
    <jsp:forward page="login.jsp"/>
</c:if>

<jsp:useBean id="e" class="HRManager.entity.Employee" scope="request"/>
<jsp:useBean id="s" class="HRManager.entity.Salary" scope="request"/>

<jsp:include page="header.jsp"/>
<!-- Start content -->

<div class="content">
    <div class="container-fluid">
        <h3 align="center">Salary Manager</h3>
        <form class="col-md-6 mx-auto" name="frmSalary" method="POST" action="salaryManager?id=${e.employeeID}">

            <div class="form-group">
                <label for="eCode">Employee Code</label>
                <input disabled class="form-control" id="eCode" type="text" name="employeeID" value="${e.employeeID}" />
            </div>

            <div class="form-group">
                <label for="fullName">Full Name</label>
                <input disabled class="form-control" id="fullName" type="text" name="fullName" value="${e.firstName} ${e.lastName}" />
            </div>

            <div class="form-group">
                <label for="birthday">Birthday</label>
                <input disabled class="form-control" id="birthday" type="text" name="birthday" value="${e.birthDate}" />
            </div>

            <div class="form-group">
                <label for="hireDate">Hire Date</label>
                <input disabled class="form-control" id="hireDate" type="text" name="hireDate" value="${e.hireDate}" />
            </div>

            <div class="form-group">
                <label for="salary">Salary</label>
                <input class="form-control" id="salary" type="number" name="salary" value="${s.salary}"/>
            </div>

            <div class="form-group">
                <input class="btn btn-primary btn-block btn-search" type="submit" name="btnSearch" value="Update">
            </div>

        </form>
    </div>
</div>

<!-- END content -->
<jsp:include page="footer.jsp"/>